package main

import (
	"database/sql"
	"fmt"
	_ "github.com/denisenkom/go-mssqldb"
	"github.com/joho/godotenv"
	"github.com/uptrace/bun"
	"github.com/uptrace/bun/dialect/mssqldialect"
	"log"
	"os"
	"time"
)

// add some text

type Answer struct {
	ID       string
	FQDN     string
	Hostname string
}

func main() {
	err := godotenv.Load("/app/config/.env")
	if err != nil {
		fmt.Errorf("Я не открыть файл")
	}

	server := os.Getenv("MSSQL_SERVER")
	port := os.Getenv("MSSQL_PORT")
	database := os.Getenv("MSSQL_DB")
	user := os.Getenv("MSSQL_USER")
	password := os.Getenv("MSSQL_PASSWORD")

	connString := fmt.Sprintf("server=%s;port=%s;database=%s;user id=%s;password=%s", server, port, database, user, password)

	sqldb, err := sql.Open("mssql", connString)
	if err != nil {
		log.Fatalf("Error opening connection to MSSQL server: %v", err)
	}
	db := bun.NewDB(sqldb, mssqldialect.New())
	var result []Answer
	pingErr := db.Ping()
	if pingErr != nil {
		log.Fatal(pingErr)
	}

	rows, err := db.Query("SELECT strName 'id', CASE WHEN (wstrDnsName IS NOT NULL) AND (wstrDnsName <> '') AND (wstrDnsDomain IS NOT NULL) AND (wstrDnsDomain <> '') THEN LOWER(wstrDnsName) + '.' + LOWER(wstrDnsDomain) ELSE '' END 'fqdn', LOWER(wstrDisplayName) 'hostname' FROM v_akpub_host WHERE tmLastVisible IS NOT NULL;")
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()
	for rows.Next() {
		var ans Answer
		err := rows.Scan(&ans.ID, &ans.FQDN, &ans.Hostname)
		if err != nil {
			log.Fatal(err)
		}
		result = append(result, ans)
	}
	fmt.Println(result)
	fmt.Println("Выполнение окончено!")
	time.Sleep(100 * time.Second)
}
